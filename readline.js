
const readline = require('readline');
const fs = require('fs');
const os = require('os');

const cpuCount = os.cpus().length

const rl = readline.createInterface({
    input: process.stdin, 
    output: process.stdout
});

const bytesToSize = (bytes) => {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    };

const message = `
Choose an option:
1. Read my package.json
2. Get my OS info
3. Start a HTTP server
-----------------------
Type a number: `

rl.question(message, (answer) =>{
    console.log('You chose option: ' + answer);

    if ( answer == 1 ){
        console.log('Reading packages.json file',)
        fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
            console.log(content);
        })


    } else if ( answer == 2){
        console.log('Getting OS info...');
        console.log('FREE MEMORY : ', bytesToSize(os.freemem()));
        console.log('SYSTEM MEMORY : ', bytesToSize(os.totalmem()));
        console.log( 'CPU CORES: ', cpuCount);
        console.log( 'ARCH: ', os.arch()); 
        console.log("PLATFORM: " + os.platform());
        console.log('RELEASE:', os.release());
        console.log('USER:', os.userInfo().username);


    } else if ( answer == 3) {
        console.log('Starting HTTP Server...');
        console.log('Listening on port 3000...'); 
        require('./http.js');

    } else  if ( answer > 3) {
        console.log('No option to match. Try a valid answer....');

    } else if ( answer == 0) {
        console.log('No option to match. Try a valid answer....');

    }
    
    rl.close();

});