const fs = require('fs');
const eol =  require('os').EOL;



fs.readFile(__dirname + '/mytext.csv', 'utf-8', (err, contents) => {

    //console.log('Error: ', err);
    //console.log('File contents: ', contents);

    const rows = contents.split(eol);
    console.log(rows)

    const quotes = rows.map( row => {

       const cols = row.split(',');
       return {
           name: cols[0],
           quote1: cols[1],
           quote2: cols[2]
       };

    });

    console.log(quotes);

});